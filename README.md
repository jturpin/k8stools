# K8S Tools

## How to run

```shell
docker run --rm -ti \
  -v ~/.kube/config:/kubeconfig \
  -v ~/.aws/credentials:/root/.aws/credentials \
  registry.gitlab.com/jturpin/k8stools
```

## What's included

* kubectl
* gcloud
* awscli
* helm v2
* helm v3
* k9s
* kubectx / kubens
* fzf
* ping / telnet / python / mysql-client
* ...
